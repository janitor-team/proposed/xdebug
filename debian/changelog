xdebug (3.1.5+2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 3.1.5+2.9.8+2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Sat, 25 Jun 2022 08:07:17 +0200

xdebug (3.1.4+2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 3.1.4+2.9.8+2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Thu, 21 Apr 2022 17:52:21 +0200

xdebug (3.1.2+2.9.8+2.8.1+2.5.5-4) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:39 +0100

xdebug (3.1.2+2.9.8+2.8.1+2.5.5-3) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:10:05 +0100

xdebug (3.1.2+2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 3.1.2+2.9.8+2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Fri, 31 Dec 2021 09:52:32 +0100

xdebug (3.1.1+2.9.8+2.8.1+2.5.5-3) unstable; urgency=medium

  * Fix d/control
  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:26:15 +0100

xdebug (3.1.1+2.9.8+2.8.1+2.5.5-2) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:01:12 +0100

xdebug (3.1.1+2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 3.1.1+2.9.8+2.8.1+2.5.5
   + xdebug 2.5.5 is used for PHP 5.6
   + xdebug 2.8.1 is used for PHP 7.0
   + xdebug 2.9.8 is used for PHP 7.1
   + xdebug 3.1.1 is used for PHP 7.2-8.0

 -- Ondřej Surý <ondrej@debian.org>  Sun, 24 Oct 2021 15:58:21 +0200

xdebug (3.0.4+2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 3.0.4+2.9.8+2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Fri, 04 Jun 2021 23:11:53 +0200

xdebug (3.0.3+2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 3.0.3+2.9.8+2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Thu, 04 Mar 2021 17:55:20 +0100

xdebug (3.0.2+2.9.8+2.8.1+2.5.5-6) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:08:36 +0100

xdebug (3.0.2+2.9.8+2.8.1+2.5.5-5) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:58 +0100

xdebug (3.0.2+2.9.8+2.8.1+2.5.5-4) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:51:37 +0100

xdebug (3.0.2+2.9.8+2.8.1+2.5.5-3) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:11:17 +0100

xdebug (3.0.2+2.9.8+2.8.1+2.5.5-2) unstable; urgency=medium

  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:41:53 +0100

xdebug (3.0.2+2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 3.0.2+2.9.8+2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Wed, 13 Jan 2021 12:16:23 +0100

xdebug (3.0.1+2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 3.0.1+2.9.8+2.8.1+2.5.5
   + xdebug 2.5.5 is used for PHP 5.6
   + xdebug 2.8.1 is used for PHP 7.0
   + xdebug 2.9.8 is used for PHP 7.1
   + xdebug 3.0.1 is used for PHP 7.2-8.0

 -- Ondřej Surý <ondrej@debian.org>  Mon, 07 Dec 2020 13:05:41 +0100

xdebug (2.9.8+2.8.1+2.5.5+3.0.0~beta1-2) unstable; urgency=medium

  * Change the package-8.xml, so it builds the version for 8.0

 -- Ondřej Surý <ondrej@debian.org>  Wed, 21 Oct 2020 20:53:18 +0200

xdebug (2.9.8+2.8.1+2.5.5+3.0.0~beta1-1) unstable; urgency=medium

  * New upstream version 2.9.8+2.8.1+2.5.5+3.0.0~beta1
   + xdebug 2.5.5 is used for PHP 5.6
   + xdebug 2.8.1 is used for PHP 7.0
   + xdebug 3.0.0~beta1 is used for PHP 8.0
   + xdebug 2.9.8 is used for everything else

 -- Ondřej Surý <ondrej@debian.org>  Sat, 17 Oct 2020 06:10:50 +0200

xdebug (2.9.8+2.8.1+2.5.5-2) unstable; urgency=medium

  * Change the package-7.xml, so it builds the version for 7.4

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Oct 2020 09:17:18 +0200

xdebug (2.9.8+2.8.1+2.5.5-1) unstable; urgency=medium

  * Finish conversion to debhelper compat level 10
  * Add epoch to the Breaks (Closes: #953727)
  * Update d/gbp.conf for main branch
  * New upstream version 2.9.8+2.8.1+2.5.5
  * Update for dh-php >= 2.0 support

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Oct 2020 20:43:51 +0200

xdebug (2.9.6+2.8.1+2.5.5-4) unstable; urgency=medium

  * Break the php-common binary package and not php-defaults source
    package (Closes: #953727)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 18 Jun 2020 10:54:31 +0200

xdebug (2.9.6+2.8.1+2.5.5-3) unstable; urgency=medium

  * Bump dh_compat to 10
  * Silence lintian warning about pkg-php-tools

 -- Ondřej Surý <ondrej@debian.org>  Thu, 11 Jun 2020 09:20:11 +0200

xdebug (2.9.6+2.8.1+2.5.5-2) unstable; urgency=medium

  * Fix the version on Breaks: php-defaults (Closes: #953727)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 11 Jun 2020 09:10:16 +0200

xdebug (2.9.6+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.9.6+2.8.1+2.5.5
  * Add Breaks: php-defaults (<= 69~) to unbreak php-codecoverage
    (Closes: #953727)

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Jun 2020 08:35:31 +0200

xdebug (2.9.5+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.9.5+2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Mon, 18 May 2020 10:14:12 +0200

xdebug (2.9.3+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.9.3+2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Sat, 21 Mar 2020 11:31:01 +0100

xdebug (2.9.2+2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.9.2+2.8.1+2.5.5
    - xdebug 2.5.5 for PHP 5.6
    - xdebug 2.8.1 for PHP 7.0
    - xdebug 2.9.2 for everything else

 -- Ondřej Surý <ondrej@debian.org>  Tue, 18 Feb 2020 17:15:13 +0100

xdebug (2.9.1+2.9.0+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.9.1+2.9.0+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Tue, 21 Jan 2020 09:14:00 +0100

xdebug (2.9.1+2.5.5-2) unstable; urgency=medium

  * Downgrade PHP minimum version

 -- Ondřej Surý <ondrej@debian.org>  Mon, 20 Jan 2020 23:41:05 +0100

xdebug (2.9.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.9.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Mon, 20 Jan 2020 17:37:59 +0100

xdebug (2.9.0+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.9.0+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 12:16:14 +0100

xdebug (2.8.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.8.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Wed, 04 Dec 2019 09:58:47 +0100

xdebug (2.8.0+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.8.0+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Sun, 17 Nov 2019 03:14:17 +0100

xdebug (2.7.2+2.5.5+2.8.0~beta2-1) unstable; urgency=medium

  * New upstream version 2.7.2+2.5.5+2.8.0~beta2
  * Add PHP 7.4 support by adding xdebug 2.8.0~beta2

 -- Ondřej Surý <ondrej@debian.org>  Tue, 29 Oct 2019 15:28:43 +0100

xdebug (2.7.2+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.7.2+2.5.5

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 09:08:57 +0200

xdebug (2.7.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.7.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Apr 2019 06:50:21 +0000

xdebug (2.7.0+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.7.0+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Thu, 07 Mar 2019 19:52:32 +0000

xdebug (2.7.0~rc2+2.6.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.7.0~rc2+2.6.1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Sun, 17 Feb 2019 13:38:00 +0000

xdebug (2.7.0~rc1+2.6.1+2.5.5-1) unstable; urgency=medium

  * Add Pre-Depends on php-common >= 0.69~
  * New upstream version 2.7.0~rc1+2.6.1+2.5.5 (Closes: #921060)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 03 Feb 2019 09:22:57 +0000

xdebug (2.7.0~beta1+2.6.1+2.5.5-2) unstable; urgency=medium

  * Bump the required dh-php version to >= 0.33~

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Oct 2018 12:24:46 +0000

xdebug (2.7.0~beta1+2.6.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.7.0~beta1+2.6.1+2.5.5
  * Use different versions of xdebug for different PHP:
    + xdebug 2.7.0~beta1 for PHP 7.3.0
    + xdebug 2.6.1 for PHP 7.0, 7.1 and 7.2
    + xdebug 2.5.5 for PHP 5.6

 -- Ondřej Surý <ondrej@debian.org>  Mon, 15 Oct 2018 11:55:03 +0000

xdebug (2.7.0~beta1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.7.0~beta1+2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Tue, 09 Oct 2018 13:01:59 +0000

xdebug (2.6.1+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.6.1+2.5.5
  * Update Vcs-* to salsa.d.o
  * Update maintainer email to team+php-pecl@tracker.debian.org
    (Closes: #899730)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 13:50:29 +0000

xdebug (2.6.0+2.5.5-1) unstable; urgency=medium

  * New upstream version 2.6.0+2.5.5
    + xdebug-2.5.5 is being used only for PHP 5.x
    + xdebug-2.6.0 is for PHP 7.x
  * PHP 5.x support will be dropped in some future release
    of the package to align with upstream.

 -- Ondřej Surý <ondrej@debian.org>  Wed, 31 Jan 2018 21:29:22 +0000

xdebug (2.6.0~rc1.1-1) unstable; urgency=medium

  * New upstream version 2.6.0~rc1.1

 -- Ondřej Surý <ondrej@debian.org>  Tue, 23 Jan 2018 13:49:45 +0000

xdebug (2.6.0~rc1-1) unstable; urgency=medium

  * New upstream version 2.6.0~rc1
  * Xdebug 2.6.0~rc1 has support for PHP 7.2, so don't disable it

 -- Ondřej Surý <ondrej@debian.org>  Tue, 23 Jan 2018 11:29:30 +0000

xdebug (2.5.5-3) unstable; urgency=medium

  * Fix max << PHP 7.2.0 dependency

 -- Ondřej Surý <ondrej@debian.org>  Thu, 06 Jul 2017 19:42:31 +0200

xdebug (2.5.5-2) unstable; urgency=medium

  * Disable PHP 7.2 in package.xml

 -- Ondřej Surý <ondrej@debian.org>  Thu, 06 Jul 2017 16:38:21 +0200

xdebug (2.5.5-1) unstable; urgency=medium

  * New upstream version 2.5.5

 -- Ondřej Surý <ondrej@debian.org>  Wed, 28 Jun 2017 22:13:49 +0200

xdebug (2.5.4-1) unstable; urgency=medium

  * New upstream version 2.5.4

 -- Ondřej Surý <ondrej@debian.org>  Mon, 29 May 2017 10:29:11 +0200

xdebug (2.5.1-1) unstable; urgency=medium

  * New upstream version 2.5.1

 -- Ondřej Surý <ondrej@debian.org>  Thu, 16 Mar 2017 22:45:13 +0100

xdebug (2.5.0-1) unstable; urgency=medium

  * Imported Upstream version 2.5.0

 -- Ondřej Surý <ondrej@debian.org>  Mon, 05 Dec 2016 11:19:21 +0100

xdebug (2.5.0~rc1-1) unstable; urgency=medium

  * Imported Upstream version 2.5.0~rc1
  * Compile xdebug 2.5.0~rc1 also for PHP 7.1

 -- Ondřej Surý <ondrej@debian.org>  Fri, 02 Dec 2016 15:15:58 +0100

xdebug (2.4.1-2) unstable; urgency=medium

  * Exclude PHP 7.1 using package.xml declarations

 -- Ondřej Surý <ondrej@debian.org>  Wed, 12 Oct 2016 17:14:06 +0200

xdebug (2.4.1-1) unstable; urgency=medium

  * Imported Upstream version 2.4.1
  * PHP 7.1 is not supported yet, remove it from the version list

 -- Ondřej Surý <ondrej@debian.org>  Mon, 29 Aug 2016 08:31:17 +0200

xdebug (2.4.0-5) unstable; urgency=medium

  * Add Vcs-* links
  * Update web git browser to https cgit and fix ancient-autotools/libtool
    lintian override
  * Update gbp branches back to master and upstream

 -- Ondřej Surý <ondrej@debian.org>  Tue, 03 May 2016 13:05:25 +0200

xdebug (2.4.0-4) unstable; urgency=medium

  * Adjust variable override ordering in d/rules
  * Adjust d/rules for _OVERRIDE variables
  * Bump required dh_php to 0.15~

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Apr 2016 13:30:24 +0200

xdebug (2.4.0-3) unstable; urgency=medium

  * Move the default makefile snippet to dh-php and use a simple d/rules
    with dh-php >= 0.12~

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Apr 2016 10:12:43 +0200

xdebug (2.4.0-2) unstable; urgency=medium

  * Improve d/rules so it can be used with both single or multiple PECL
    upstream versions, controlled by looking for package-MAJOR.MINOR.xml,
    package-MAJOR.xml and package.xml in this order

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Apr 2016 13:49:39 +0200

xdebug (2.4.0-1) unstable; urgency=medium

  * Imported Upstream version 2.4.0

 -- Ondřej Surý <ondrej@debian.org>  Fri, 04 Mar 2016 10:08:03 +0100

xdebug (2.4.0~rc4-2) unstable; urgency=medium

  * Force rebuild with dh_php >= 0.7
  * wrap-and-sortize d/control and d/copyright

 -- Ondřej Surý <ondrej@debian.org>  Mon, 29 Feb 2016 22:35:18 +0100

xdebug (2.4.0~rc4-1) unstable; urgency=medium

  * Imported Upstream version 2.4.0~rc4

 -- Ondřej Surý <ondrej@debian.org>  Tue, 26 Jan 2016 12:52:31 +0100

xdebug (2.4.0~rc3-1) unstable; urgency=medium

  * Imported Upstream version 2.4.0~rc3
  * Implement a dual PHP 5.6 and 7.0 build for PHP xdebug extension

 -- Ondřej Surý <ondrej@debian.org>  Wed, 16 Dec 2015 15:20:16 +0100

xdebug (2.4.0~rc2-1) unstable; urgency=medium

  * Add gbp.conf for PHP 7.0
  * Imported Upstream version 2.4.0~rc2
  * Update xdebug to support PHP 7.0 builds

 -- Ondřej Surý <ondrej@debian.org>  Tue, 08 Dec 2015 15:40:50 +0100

xdebug (2.3.2-1) unstable; urgency=medium

  * New upstream version 2.3.2

 -- Ondřej Surý <ondrej@debian.org>  Sat, 02 May 2015 13:15:35 +0200

xdebug (2.2.6-1) unstable; urgency=medium

  * New upstream version 2.2.6

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Jan 2015 09:36:17 +0100

xdebug (2.2.5-1) unstable; urgency=medium

  * New upstream version 2.2.5

 -- Ondřej Surý <ondrej@debian.org>  Thu, 15 May 2014 11:19:19 +0200

xdebug (2.2.4-1) unstable; urgency=medium

  * New upstream release

 -- Lior Kaplan <kaplan@debian.org>  Mon, 07 Apr 2014 01:41:25 +0300

xdebug (2.2.3-2) unstable; urgency=low

  * Update the build system to use dh-php5
  * Move under the PHP PECL team umbrella

 -- Ondřej Surý <ondrej@debian.org>  Tue, 22 Oct 2013 14:55:27 +0200

xdebug (2.2.3-1) unstable; urgency=low

  * New upstream release (Closes: #710530)
    - Adds support for PHP 5.5

 -- Lior Kaplan <kaplan@debian.org>  Fri, 31 May 2013 21:13:27 +0300

xdebug (2.2.1-2) unstable; urgency=low

  * Force correct path to xdebug.so in conffile (Closes: #689791)

 -- Lior Kaplan <kaplan@debian.org>  Sun, 11 Nov 2012 00:28:28 +0200

xdebug (2.2.1-1) unstable; urgency=low

  * New upstream release
  * Move dpkg-maintscript-helper call from prerm to postrm (Closes: #684266)

 -- Lior Kaplan <kaplan@debian.org>  Sat, 11 Aug 2012 03:08:22 +0300

xdebug (2.2.0-1) unstable; urgency=low

  * New upstream release
  * Manage ini file with php5enmod and php5enmod (Closes: #667788) 

 -- Lior Kaplan <kaplan@debian.org>  Sun, 20 May 2012 22:47:32 +0300

xdebug (2.2.0~rc1-1) unstable; urgency=low

  * New upstream release
    - Adds support for PHP 5.4 (Closes: #656491)

 -- Lior Kaplan <kaplan@debian.org>  Thu, 15 Mar 2012 19:48:56 +0200

xdebug (2.1.3-2) unstable; urgency=low

  * Drop php 5.4 support patches as they cause problems (Closes: #657883)

 -- Lior Kaplan <kaplan@debian.org>  Mon, 30 Jan 2012 23:26:52 +0200

xdebug (2.1.3-1) unstable; urgency=low

  * New upstream release
  * Remove PHP < 5.4 version check in configure.
  * Adapt cherry-picked patches for PHP 5.4 support.

 -- Lior Kaplan <kaplan@debian.org>  Thu, 26 Jan 2012 01:51:05 +0200

xdebug (2.1.2-1) unstable; urgency=low

  * New upstream release
  * Cherry-picked 413d88b56 from upstream for PHP 5.4 support (Closes: #656491)
  * Cherry-picked 1ef642b39 from upstream for having PHP 5.3 and 5.4 support
  * Cherry-picked 04f80138f from upstream for php 5.4 ZEND_RETURN_BY_REF support
  * Fix lintian warning duplicate-changelog-files
  * Bumped Standards version (no changes required)
  * Add myself to Uploaders

 -- Lior Kaplan <kaplan@debian.org>  Tue, 24 Jan 2012 01:49:28 +0200

xdebug (2.1.0-1) unstable; urgency=low

  * New upstream release (Closes: #570163, #571765, #555643, #582937)
  * Switch to dpkg-source 3.0 (quilt) format
  * Bumped Standards version

 -- Martin Meredith <mez@debian.org>  Thu, 08 Jul 2010 23:23:38 +0100

xdebug (2.0.5-1) unstable; urgency=low

  * New upstream release
  * Bumped Standards Version 

 -- Martin Meredith <mez@debian.org>  Mon, 27 Jul 2009 21:27:36 +0100

xdebug (2.0.4-2) unstable; urgency=low

  * Fix broken watch file (which somehow went missing from the previous upload)

 -- Martin Meredith <mez@debian.org>  Mon, 12 Jan 2009 00:44:43 +0000

xdebug (2.0.4-1) unstable; urgency=low

  * New upstream release
  * Fixed watch file for new download location
  * Removed Uploader (have never had any contact from him)
  * Changed Maintainer (for new Debian Address)
  * Updated Standards Version
  * Changed php5-dev depends (can build with 5.1 or greater)

 -- Martin Meredith <mez@debian.org>  Wed, 07 Jan 2009 19:48:57 +0000

xdebug (2.0.3-1) unstable; urgency=low

  * Initial release (Closes: #377348)

 -- Martin Meredith <mez@ubuntu.com>  Tue, 08 Apr 2008 15:25:16 +0100

